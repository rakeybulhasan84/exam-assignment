<?php

namespace Config;

class Database {
    /**
     * Database host
     *
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     *
     * @var string
     */
    const DB_NAME = 'exam';

    /**
     * Database username
     *
     * @var string
     */
    const DB_USER = 'root';

    /**
     * Database password
     *
     * @var string
     */
    const DB_PASSWORD = '12345678';
}
